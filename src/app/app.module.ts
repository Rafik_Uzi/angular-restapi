import { NgModule, APP_INITIALIZER, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { DropdownModule } from 'primeng/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CinemaComponent } from './Page/cinema/cinema.component';
import { CinemaDetailsComponent } from './Page/cinema-details/cinema-details.component';
import { CinemaScreenComponent } from './Page/cinema-screen/cinema-screen.component';
import { ProgressBarModule } from 'primeng/progressbar';


@NgModule({
  declarations: [AppComponent, CinemaComponent, CinemaDetailsComponent, CinemaScreenComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    DropdownModule,
    BrowserAnimationsModule,
    FormsModule,
    ProgressBarModule,
    RouterModule,
  ],
  exports: [CinemaScreenComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
