import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable, throwError } from "rxjs";
import { Cinema, ScreenCinema } from "../Model/model";
import { ScreenSeat } from "../Model/screen-seat";
import { environment } from "../envirnoments/envirnoments";

@Injectable({
  providedIn: "root",
})
export class HttpService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = environment.apiurl;
  }

  GetCinemas(): Observable<Cinema[]> {
    return this.http
      .get<Cinema[]>(`${this.url}/cinema`)
      .pipe(catchError(this.HandleError));
  }

  GetCinema(cinemaid: string): Observable<Cinema> {
    return this.http.get<Cinema>(`${this.url}/cinema/${cinemaid}`)
  }

  GetScreen(cinemaid: string): Observable<ScreenCinema>{
    return this.http.get<ScreenCinema>(`${this.url}/cinema/${cinemaid}/screen`).pipe(catchError(this.HandleError));
  }

  GetSeat(cinemaid: string, screenid: string): Observable<ScreenSeat>{
    return this.http.get<ScreenSeat>(`${this.url}/cinema/${cinemaid}/screen/${screenid}`).pipe(catchError(this.HandleError))
  }


  private HandleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Blad po stronie klineta
      console.error("cos jest nie tak ", error.error.message);
    } else {
      // blad zwracany po stronie serwera
      console.error(
        `Zwarcany kod bledu ${error.status}, ` +
          `problem wystepuje w: ${error.error}`
      );
    }
    return throwError("Wystapil problem z aplikacja prosze zrestartowac");
  }
}
