import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CinemaComponent} from './Page/cinema/cinema.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: '/cinema',
    pathMatch: 'full',
  },
  { path: 'cinema', component: CinemaComponent },
  { path: 'cinema/:id', component: CinemaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
