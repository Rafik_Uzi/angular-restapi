export interface Cinema {
  id:                    string;
  name:                  string;
  groupId:               string;
  city:                  string;
  province:              string;
  zipcode:               string;
  street:                string;
  latitude:              number;
  longitude:             number;
  phone:                 string;
  reservationPhone:      string;
  description:           string;
  url1:                  string;
  url2:                  string;
  numberOfScreens:       number;
  numberOfSeats:         number;
  numberOfDisabledSeats: null;
  graphics:              any[];
  email:                 string;
  reservationGroupPhone: string;
  fax:                   string;
  isAnyDreamScreen:      boolean;
  taxId:                 string;
  euTaxId:               string;
  companyName:           string;
  ffaNumber:             string;
}

export interface ScreenCinema {
  forEach(arg0: (element: any) => void);
  map(arg0: (seat: any) => {}): any;
  id:                     string;
  name:                   string;
  type:                   null;
  feature:                Feature;
  number:                 number;
  rows:                   Row[];
  cols:                   Col[];
  seats:                  Seat[];
  groups:                 Group[];
  defaultSeatDescription: null;
  defaultSeatGroupName:   null;
  blockedList:            any[];
  screenGroupId:          null;
  ffaNumber:              null | string;
  proCinema:              null | string;
  screenElements:         any[];
}

export interface Col {
  id:         string;
  coordinate: number;
}

export enum Feature {
  ReserveSeatingHeatedRecliners = "Reserve Seating,Heated Recliners",
}

export interface Group {
  seatIds:     string[];
  id:          string;
  ico:         string;
  color:       Color | null;
  name:        Name;
  description: string;
  type:        Type;
}

export enum Color {
  F2Dcdb = "#f2dcdb",
}

export enum Name {
  Companion = "Companion",
  Wheelchair = "wheelchair",
}

export enum Type {
  Companion = "companion",
  Wheelchair = "wheelchair",
}

export interface Row {
  id:         string;
  legend:     string;
  coordinate: number;
}

export interface Seat {
  id:      string;
  symbol:  string;
  kind:    string;
  colId:   string;
  rowId:   string;
  groupId: null | string;
}


export interface cin {
  id: string;
  name: string;
}