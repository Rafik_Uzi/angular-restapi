export interface ScreenSeat {
    find(arg0: (o: any) => boolean): import("./model").ScreenCinema;
    id:                     string;
    name:                   string;
    type:                   string;
    feature:                Feature;
    number:                 number;
    rows:                   Row[];
    cols:                   Col[];
    seats:                  Seat[];
    groups:                 Group[];
    defaultSeatDescription: string;
    defaultSeatGroupName:   string;
    blockedList:            any[];
    screenGroupId:          null;
    ffaNumber:              string;
    proCinema:              string;
    screenElements:         ScreenElement[];
}

export interface Col {
    id:         string;
    coordinate: number;
}

export enum Feature {
    LoungeNameEN = "Lounge - name EN",
    SofaNameEN = "Sofa - name EN",
    WheelchairNameEN = "Wheelchair - name EN",
    Xperience = "Xperience",
}

export interface Group {
    seatIds:     string[];
    id:          string;
    ico:         string;
    color:       Color | null;
    name:        Feature;
    description: Description;
    type:        Type;
}

export enum Color {
    The76923C = "#76923c",
}

export enum Description {
    Empty = "",
    LoungePOSMessage = "Lounge - POS Message",
    SofaPOSMessage = "Sofa - POS Message",
    WheelchairPOSMessage = "Wheelchair - POS Message",
}

export enum Type {
    Couch = "couch",
    Lounge = "lounge",
    Wheelchair = "wheelchair",
    Xperience = "xperience",
}

export interface Row {
    id:         string;
    legend:     string;
    coordinate: number;
}

export interface ScreenElement {
    id:      string;
    name:    string;
    seatIds: string[];
}

export interface Seat {
    id:      string;
    symbol:  string;
    kind:    string;
    colId:   string;
    rowId:   string;
    groupId: null | string;
}
