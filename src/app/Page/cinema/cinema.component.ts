import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import {Cinema, ScreenCinema} from 'src/app/Model/model'
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, tap } from 'rxjs';
import { ScreenSeat } from 'src/app/Model/screen-seat';

@Component({
  selector: 'app-cinema',
  templateUrl: './cinema.component.html',
  styleUrls: ['./cinema.component.css']
})
export class CinemaComponent implements OnInit {
  Cinema: Cinema = null;
  Cinemas: Cinema[];
  ScreenCinema: ScreenCinema = null;
  loadingScreen = false;
  ScreenSeat: ScreenSeat = null;

  constructor(private http: HttpService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getCinemas(this.route.snapshot.params['id']);
  }

  private getCinemas(routeCinemaId:string){
    this.http.GetCinemas().pipe(
      map((cin)=> cin.map((cinema) =>({
        ...cinema,
        name: cinema.name,
        id: cinema.id,
      })) 
    )).subscribe((data: Cinema[]) => {
      this.Cinemas = data;
      if (routeCinemaId) {
        this.Cinema = data.find(o => o.id === routeCinemaId)
        this.loadScreen(routeCinemaId);
      }
    });
  }

  private loadScreen(cinemaId) {
    this.loadingScreen = true;
    return this.http.GetScreen(cinemaId).pipe(map((s) => s.map((seat) =>({
      screenid: seat.id,
    }))))
      .subscribe(screen =>{
        this.ScreenCinema = screen
        this.ScreenCinema.forEach(screen => {
          const { screenid } = screen;
          this.http.GetSeat(cinemaId,screenid).subscribe(seat =>{
            console.log(this.ScreenSeat = seat)
          })
          })
      })
  }

  change(): void {
    this.clear(false);

    if (this.Cinema) {
      this.loadScreen(this.Cinema.id);
      window.history.replaceState({}, '',`/cinema/${this.Cinema.id}`);
    }
  }

  clear(changeHistory = true) {
    this.ScreenCinema = null;
    this.loadingScreen = false;

    if (changeHistory) {
      window.history.replaceState({}, '',`/cinema`);
    }
  }
}
