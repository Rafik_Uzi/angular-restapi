import { Component, Input, OnInit } from '@angular/core';
import { Cinema, ScreenCinema } from 'src/app/Model/model';
import { ScreenSeat } from 'src/app/Model/screen-seat';

@Component({
  selector: 'app-cinema-details',
  templateUrl: './cinema-details.component.html',
  styleUrls: ['./cinema-details.component.css']
})
export class CinemaDetailsComponent implements OnInit {
  @Input() cinema: Cinema = null;
  @Input() screen: ScreenCinema = null;
  @Input() seat: ScreenSeat = null;
  @Input() screenLoading: boolean = false;
  
  constructor() { }

  ngOnInit(): void {
  }

}
